colorscheme NeoSolarized
set termguicolors

syntax on
set expandtab
set shiftwidth=4
set softtabstop=4
set tabstop=4
set backspace=indent,eol,start "backspace over anything
set incsearch
set hlsearch
set mouse=a
set smartcase
set foldmethod=marker

filetype plugin indent on
set spellsuggest=best,10
set spell

set backupdir=~/.vim/.backup//
set directory=~/.vim/.swp//

" -- coc config -- {{{

" TextEdit might fail if hidden is not set.
set hidden
" Having longer updatetime (default is 4000 ms = 4 s) leads to noticeable
" delays and poor user experience.
set updatetime=300
" Don't pass messages to |ins-completion-menu|.
set shortmess+=c


" Use tab for trigger completion with characters ahead and navigate.
" Use command ':verbose imap <tab>' to make sure tab is not mapped by other plugin.
inoremap <silent><expr> <TAB>
  \ coc#pum#visible() ? coc#pum#next(1):
  \ <SID>check_back_space() ? "\<Tab>" :
  \ coc#refresh()
inoremap <expr><S-TAB> coc#pum#visible() ? coc#pum#prev(1) : "\<C-h>"

function! s:check_back_space() abort
  let col = col('.') - 1
  return !col || getline('.')[col - 1]  =~ '\s'
endfunction

" Use <c-space> to trigger completion.
inoremap <silent><expr> <c-space> coc#refresh()

" Use <CR> to confirm completion
inoremap <expr> <cr> coc#pum#visible() ? coc#_select_confirm() : "\<CR>"

nmap <silent> <leader>k <Plug>(coc-diagnostic-info)
nmap <silent> <leader>p <Plug>(coc-diagnostic-prev)
nmap <silent> <leader>n <Plug>(coc-diagnostic-next)

" Remap keys for gotos
nmap <silent> <leader>fd <Plug>(coc-definition)
nmap <silent> <leader>fy <Plug>(coc-type-definition)
nmap <silent> <leader>fi <Plug>(coc-implementation)
nmap <silent> <leader>fr <Plug>(coc-references)

"Remap for format selected region
nmap <silent> <leader>ff <Plug>(coc-format-selected)
xmap <silent> <leader>ff <Plug>(coc-format-selected)

" Add `:Format` command to format current buffer
command! -nargs=0 Format :call CocActionAsync('format')


" Symbol renaming.
nmap <leader>rn <Plug>(coc-rename)


nnoremap <silent> <leader>t :call CocActionAsync('doHover')<cr>

" Use K to show documentation in preview window
nnoremap <silent> K :call <SID>show_documentation()<CR>

function! s:show_documentation()
  if (index(['vim','help'], &filetype) >= 0)
    execute 'h '.expand('<cword>')
  else
    call CocAction('doHover')
  endif
endfunction

" Set statusline
set statusline=%<%F\ %{coc#status()}\ %h%m%r%=%-14.(%l,%c%V%)\ %P
set laststatus=2
" }}}

" Open new split panes to right and bottom, which feels more natural
" set splitbelow
set splitright
set scrolloff=8     " Start scrolling when we're 8 lines away from margins
set autowrite       " Automatically :write before running commands

set cursorline
set relativenumber
set number
" Toggle relative numbering, and set to absolute on loss of focus or insert mode
autocmd FocusLost * :set norelativenumber
autocmd FocusGained * :set relativenumber
autocmd InsertEnter * :set norelativenumber
autocmd InsertLeave * :set relativenumber

" Use tab to jump between blocks, because it's easier
nnoremap <tab> %
vnoremap <tab> %

" Always use vertical diffs
set diffopt+=vertical
" Ignore whitespace in diffs
set diffopt+=iwhite


let mapleader = "ä"
let maplocalleader = "Ã"


" Ctrlp config ----------------- {{{
"let g:ctrlp_by_filename = 1
let g:ctrlp_working_path_mode = 'a'
let g:ctrlp_match_window = 'max:20'
let g:ctrlp_open_multiple_files = '4vjr'
let g:ctrlp_custom_ignore = {
    \ 'dir': 'target.*'
    \ }
" }}}

set pastetoggle=<F2>

nnoremap ö :
inoremap jk <ESC>
cnoremap jk <ESC>
nnoremap , ;
nnoremap ; ,

" Change current word to uppercase
nnoremap <leader>u viwU

" Replace current word with yanked
nnoremap <leader>r viw"0p
vnoremap <leader>r "0p
nnoremap <leader>p "0p
nnoremap <leader>P "0P
vnoremap iq i"
vnoremap aq a"

nnoremap <leader>ev :vsplit $MYVIMRC<cr>
nnoremap <leader>sv :source $MYVIMRC<cr>

if has("gui_running")
  map  <silent>  <S-Insert>  "*p
  imap <silent>  <S-Insert>  <Esc>"*pa
endif

" Quote word
nnoremap <leader>q ciw""<esc>P
vnoremap <leader>q c""<esc>P

"next match and center screen
nnoremap <c-n> :cn<cr>zz

onoremap iq i"
onoremap inq :<c-u>normal! f"vi"<cr>
onoremap aq a"
onoremap anq :<c-u>normal! f"va"<cr>
onoremap iQ i'
onoremap inQ :<c-u>normal! f'vi'<cr>
onoremap aQ a'
onoremap nf :<c-u>normal! v2f:<cr>
onoremap inb :<c-u>normal! f(vi(<cr>
onoremap ilb :<c-u>normal! F)vi(<cr>

abbreviate td TODO:
abbreviate teh the


" Disable error notifications
set noerrorbells visualbell t_vb=
augroup nobell
    autocmd!
    autocmd GUIEnter * set visualbell t_vb=
augroup END

" If doing a diff. Upon writing changes to file, automatically update the
" differences
autocmd BufWritePost * if &diff == 1 | diffupdate | endif

set wildignore+=cscope.*
set wildignore+=tags
set wildignore+=target*/**
set wildignore+=*.pyc
set wildignore+=.git/**
set wildignore+=.hg/**

" set grep command
set grepprg=grep\ -n\ $*\ --exclude-dir={target,.hg,.git,vendor}\ --exclude={cscope\\*,tags,.hgtags}\ /dev/null


" expand '%%' to current path in console
cabbr <expr> %% expand('%:p:h')
" Edit file in current path
cabbr <expr> E ((getcmdtype()==':' && getcmdpos()<=2) ? 'e ' . expand('%:p:h'): 'E')

" Disable tmux navigator when zooming the Vim pane
let g:tmux_navigator_disable_when_zoomed = 1

let g:python3_host_prog = '/usr/bin/python3'

if exists(':tnoremap')
    tnoremap jk <C-\><C-n>
    tnoremap <Esc> <C-\><C-n>

    tnoremap <silent> <c-h> <c-\><c-n>:TmuxNavigateLeft<cr>
    tnoremap <silent> <c-j> <c-\><c-n>:TmuxNavigateDown<cr>
    tnoremap <silent> <c-k> <c-\><c-n>:TmuxNavigateUp<cr>
    tnoremap <silent> <c-l> <c-\><c-n>:TmuxNavigateRight<cr>
    tnoremap <silent> <c-\> <c-\><c-n>:TmuxNavigatePrevious<cr>
endif
